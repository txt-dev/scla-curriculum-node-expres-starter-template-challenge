README

# TXT Node + Express Challenge

This is the starter tempalte for the node + express challenge.

# Installing

```
npm install
```

# Running app
    ```
    node server.js
    ```
## Authors

* **Roberto Sanchez** - roberto@urbantxt.com 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

